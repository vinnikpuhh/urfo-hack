from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from starlette import status

from app.business.user import User
from app.core.auth import RoleChecker, auth
from app.models import UserModel, ProfileModel
from app.schemas import CreateUserSchema, UserSchema, GetListUserSchema, UpdateUserSchema, ProfileSchema
from app.core.database.db import data_base
from app.utils import object_as_dict, clean_update_data
from sqlalchemy import delete, update

user_router = APIRouter(prefix='/user', tags=['User'])
allow_cud_resource = RoleChecker([UserModel.RoleEnum.admin])
allow_read_resource = RoleChecker([UserModel.RoleEnum.admin, UserModel.RoleEnum.operator])
allow_add_remove_task = RoleChecker([UserModel.RoleEnum.admin, UserModel.RoleEnum.operator])
allow_add_remove_working_tools = RoleChecker([UserModel.RoleEnum.admin])


@user_router.post(
    '/create',
    summary='create user',
    status_code=status.HTTP_200_OK,
    response_model=UserSchema,
    dependencies=[Depends(allow_cud_resource)]
)
async def create(data: CreateUserSchema):
    with data_base.session() as session:
        user = await User.create_in_db(data, session)
        return UserSchema(
            id=user.id,
            email=user.email,
            role=user.role,
            phone_number=user.phone_number,
            is_active=user.is_active,
            profile=ProfileSchema(**object_as_dict(user.profile))
        )


@user_router.put(
    '/update/{id}',
    summary='update user',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_cud_resource)]
)
async def update_user(id: UUID, data: UpdateUserSchema):
    with data_base.session() as session:
        update_data = clean_update_data(data.model_dump())
        profile = update_data.pop('profile', None)
        if profile:
            update_profile_data = clean_update_data(profile)
            if update_profile_data:
                session.execute(
                    update(ProfileModel).
                    where(ProfileModel.user_id == id).
                    values(**update_profile_data)
                )
        if not update_data:
            return

        session.execute(
            update(UserModel).
            where(UserModel.id == id).
            values(**update_data)
        )


@user_router.delete(
    '/delete/{id}',
    summary='delete user',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_cud_resource)]
)
async def delete_user(id: UUID):
    with data_base.session() as session:
        session.execute(delete(UserModel).where(UserModel.id == id))


@user_router.get(
    '/{id}',
    summary='get user',
    status_code=status.HTTP_200_OK,
    response_model=UserSchema,
    dependencies=[Depends(allow_read_resource)]
)
async def get(id: UUID):
    with data_base.session() as session:
        user = await User.get_by_id(id, session)
        return UserSchema(
            id=user.id,
            email=user.email,
            role=user.role,
            phone_number=user.phone_number,
            is_active=user.is_active,
            profile=object_as_dict(user.profile)
        )


@user_router.get(
    '/list/',
    summary='get users',
    status_code=status.HTTP_200_OK,
    response_model=GetListUserSchema,
    dependencies=[Depends(allow_read_resource)]
)
async def get_list():
    with data_base.session() as session:
        users = await User.get_all(session)
        return GetListUserSchema(
            users=[UserSchema(
                id=x.id,
                email=x.email,
                phone_number=x.phone_number,
                role=x.role,
                is_active=x.is_active,
                profile=object_as_dict(x.profile)
            ) for x in users]
        )



