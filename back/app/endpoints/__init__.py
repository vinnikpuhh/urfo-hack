from fastapi import APIRouter

from app.endpoints.auth_endpoint import auth_route
from app.endpoints.user_endpoint import user_router
from app.endpoints.work_book_page import work_book_page_router

router = APIRouter()
router.include_router(auth_route)
router.include_router(user_router)
router.include_router(work_book_page_router)
