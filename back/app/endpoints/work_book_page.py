from uuid import UUID

from fastapi import APIRouter, Depends, UploadFile, File, Form
from sqlalchemy import select
from starlette import status
from starlette.responses import FileResponse, Response

from app.core import data_base
from app.core.auth import RoleChecker
from app.models import UserModel, WorkBookPageModel
from app.schemas.work_book_page_schema import WorkBookPageSchema, WorkBookPageListSchema, \
    WorkBookPageCreateResponseSchema
from app.utils import object_as_dict

work_book_page_router = APIRouter(prefix='/work_book_page', tags=['WorkBookPage'])

allow_read_resource = RoleChecker([UserModel.RoleEnum.admin, UserModel.RoleEnum.operator])


@work_book_page_router.get(
    '/{id}',
    summary='get work book page',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_read_resource)]
)
async def get(id: UUID) -> WorkBookPageSchema:
    with data_base.session() as session:
        query = select(WorkBookPageModel).where(WorkBookPageModel.id == id)
        obj = session.scalars(query).one()
        return WorkBookPageSchema(
            **object_as_dict(obj)
        )


@work_book_page_router.get(
    '/file/{id}',
    summary='get file work book page',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_read_resource)]
)
async def file_get(id: UUID):
    with data_base.session() as session:
        query = select(WorkBookPageModel).filter_by(id=id)
        obj = session.scalars(query).one()
        return FileResponse(obj.image)


@work_book_page_router.get(
    '',
    summary='get work book pages',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_read_resource)]
)
async def list() -> WorkBookPageListSchema:
    with data_base.session() as session:
        query = select(WorkBookPageModel)
        objs = session.scalars(query).all()
        result = []
        for obj in objs:
            result.append(
                WorkBookPageSchema(
                    **object_as_dict(obj)
                )
            )

        return WorkBookPageListSchema(
            work_book_pages=result
        )


@work_book_page_router.post(
    '/create',
    summary='create work book pages',
    status_code=status.HTTP_201_CREATED,
    response_model=WorkBookPageCreateResponseSchema,
    dependencies=[Depends(allow_read_resource)]
)
async def create(
        page: WorkBookPageModel.PageEnum = Form(...),
        file: UploadFile = File(...)
):
    with data_base.session() as session:
        wb_page = WorkBookPageModel(
            page=page,
            image=file,
        )
        session.add_all([wb_page])
        session.flush()
        return WorkBookPageCreateResponseSchema(id=wb_page.id)


