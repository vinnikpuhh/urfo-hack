from datetime import datetime
from typing import Optional
from uuid import UUID

from fastapi import UploadFile, File
from pydantic import BaseModel, FileUrl

from app.models import WorkBookPageModel


class WorkBookPageSchema(BaseModel):
    id: UUID
    page: WorkBookPageModel.PageEnum
    text: str = None
    created_at: datetime = None
    updated_at: datetime = None


class WorkBookPageCreateSchema(BaseModel):
    page: WorkBookPageModel.PageEnum
    image: Optional[UploadFile] = File(...)


class WorkBookPageListSchema(BaseModel):
    work_book_pages: list[WorkBookPageSchema]


class WorkBookPageCreateResponseSchema(BaseModel):
    id: UUID