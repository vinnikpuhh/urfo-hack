import asyncio

from app.console.base import ConsoleCommand
from app.business.user import User
from app.schemas import CreateUserSchema


class CreateDefaultAdminCommand(ConsoleCommand):
    def execute(self):
        asyncio.run(
            User.create_in_db(
                CreateUserSchema(
                    email="luginich.danil@gmail.com",
                    phone_number="+79080578569",
                    password="123456789",
                    role="admin",
                    is_active=True,
                    profile={}
                )
            )
        )

        print('success')



