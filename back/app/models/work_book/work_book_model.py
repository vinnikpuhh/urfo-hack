import enum

import sqlalchemy as sa
from fastapi_storages import FileSystemStorage
from fastapi_storages.integrations.sqlalchemy import FileType
from sqlalchemy import Column
from app.core import OrmBaseModel
from app.models.mixin import UuidIdMixin, TimestampMixin


class WorkBookPageModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'work_book_page'

    class PageEnum(enum.Enum):
        first = 'first'
        second = 'second'

    image = Column(FileType(storage=FileSystemStorage(path="/srv/hack/app/media")))
    page = sa.Column(sa.Enum(PageEnum))
    text = sa.String()

