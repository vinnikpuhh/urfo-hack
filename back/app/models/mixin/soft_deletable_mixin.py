from datetime import datetime
from sqlalchemy import Column, DateTime, Boolean, func


class SoftDeletableMixin:
    is_deleted = Column(Boolean, nullable=False, default=False, server_default='False', index=True)
    updated_at = Column(DateTime, server_default=func.now(), server_onupdate=func.now(), nullable=True)

    async def delete(self):
        if not self.is_deleted:
            self.is_deleted = True
            self.updated_at = datetime.utcnow()
        await self.db_session.flush()
