import re


def validate_phone_number(phone_number: str) -> bool:
    phone_number = re.sub(r'([ \-()])', '', phone_number)
    if phone_number.startswith('+7'):
        phone_number = phone_number[2:]
    if len(phone_number) == 11 and phone_number.startswith('7'):
        phone_number = phone_number[1:]
    if len(phone_number) != 10 or not phone_number.isnumeric():
        return False
    return True


def validate_password(password):
    if len(password) > 7 and re.search(r'([A-Za-z0-9])', password):
        return True
    else:
        return False
