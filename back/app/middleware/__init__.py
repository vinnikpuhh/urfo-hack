from starlette.middleware import Middleware
from fastapi.middleware.cors import CORSMiddleware
from uvicorn.middleware.proxy_headers import ProxyHeadersMiddleware
from app.settings import ALLOW_CORS
from .validation_error_middleware import ValidationErrorMiddleware

middleware = [
    Middleware(ProxyHeadersMiddleware, trusted_hosts='*'),
    Middleware(ValidationErrorMiddleware),
]

if ALLOW_CORS:
    cors_settings = {
        'allow_origins': ['*'],
        'allow_credentials': ['*'],
        'allow_methods': ['*'],
        'allow_headers': ['*'],
    }
    middleware.insert(0, Middleware(CORSMiddleware, **cors_settings))
