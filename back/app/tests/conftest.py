import asyncio

import pytest
import pytest_asyncio
from httpx import AsyncClient
from sqlalchemy import Engine, create_engine, text, MetaData
from sqlalchemy.orm import Session


from app.core import OrmBaseModel, data_base
from app.services import Cache
from app.settings import DATABASE_URL
from app.asgi import app
from app.tests.data import DataManager
from app.models import UserModel, ProfileModel


@pytest.fixture(scope="session")
def event_loop() -> asyncio.AbstractEventLoop:
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    try:
        yield loop
    finally:
        loop.close()


@pytest.fixture(scope='session')
def database_engine() -> Engine:
    engine = create_engine(DATABASE_URL, connect_args={'options': '-csearch_path=test'})
    with engine.connect() as connection:
        connection.execute(text('CREATE SCHEMA IF NOT EXISTS test;'))
        OrmBaseModel.metadata.create_all(connection)
        connection.commit()
        try:
            yield engine
        finally:
            OrmBaseModel.metadata.drop_all(connection)
            connection.execute(text('DROP SCHEMA test;'))
            connection.commit()


@pytest.fixture(scope='session', autouse=True)
def configure_data_base(database_engine: Engine):
    data_base.configure(database_engine)


@pytest.fixture(scope='session')
def session() -> Session:
    with data_base.session() as session:
        try:
            yield session
        finally:
            session.commit()


@pytest_asyncio.fixture(scope='session', autouse=True)
async def connect_cache():
    cache = Cache()
    await cache.connect()
    try:
        yield cache
    finally:
        await cache.disconnect()


@pytest_asyncio.fixture(scope='session')
async def client() -> AsyncClient:
    async with AsyncClient(app=app, base_url='http://anything') as client:
        yield client


@pytest_asyncio.fixture(scope='session')
async def auth_client(data_manager: DataManager) -> AsyncClient:
    async with AsyncClient(app=app, base_url='http://anything') as client:
        response = await client.post('/login', data={
            'grant_type': '',
            'scope': '',
            'client_id': '',
            'client_secret': '',
            'username': data_manager.registered_user_data.get('email'),
            'password': data_manager.registered_user_data.get('password')
        })
        access_token = response.json().get('access_token', None)
        assert access_token

        client.headers['Authorization'] = 'Bearer {}'.format(access_token)
        yield client


@pytest.fixture(scope='session')
def data_manager() -> DataManager:
    return DataManager()


@pytest.fixture(scope='session', autouse=True)
def load_registered_user(session: Session, data_manager: DataManager):
    user = UserModel(
                email=data_manager.registered_user_data.get('email'),
                phone_number=data_manager.registered_user_data.get('phone_number'),
                password=data_manager.registered_user_data.get('password'),
                profile=ProfileModel()
            )
    session.add(user)
    session.flush()

