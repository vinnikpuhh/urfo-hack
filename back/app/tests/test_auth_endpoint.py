import pytest
from httpx import AsyncClient

from app.tests.data import DataManager


@pytest.mark.asyncio
async def test_registration_endpoint(auth_client: AsyncClient, data_manager: DataManager):
    response = await auth_client.get('/me')
    assert response.status_code == 200
    assert response.json().get('email') == data_manager.registered_user_data.get('email')
