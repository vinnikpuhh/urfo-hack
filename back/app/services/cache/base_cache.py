from typing import Type, Any, Union
from logging import getLogger
from datetime import timedelta
from urllib.parse import ParseResult, urlparse
from app.utils.singleton import Singleton
from app.services.cache.exceptions import CacheExceptions


logger = getLogger(__name__)


class Cache(metaclass=Singleton):

    _class: Type['Cache'] = None

    def __new__(cls, *args, **kwargs):
        if cls != Cache and issubclass(cls, Cache):
            return super().__new__(cls)
        if not cls._class:
            raise CacheExceptions('Cache is not configured')
        return cls._class()

    @classmethod
    def set_url(cls, url: str):
        parsed: ParseResult = urlparse(url)
        if parsed.scheme in ('redis', 'rediss'):
            from app.services.cache.redis_cache import RedisCache
            RedisCache.configure(url)
            cls._class = RedisCache
        else:
            logger.warning(f'Cache {cls.__name__} got url {parsed.scheme}')
            ValueError('Cache URL must specify one of the following schemes (redis://, rediss://)')

    async def connect(self):
        raise NotImplementedError

    async def disconnect(self):
        raise NotImplementedError

    async def get(self, key: str) -> Any:
        raise NotImplementedError

    async def set(self, key: str, value: Any, ttl: Union[int, timedelta, None] = None):
        raise NotImplementedError

    async def delete(self, key: str):
        raise NotImplementedError
