from setuptools import setup, find_packages

requires = [
    'annotated-types==0.5.0',
    'anyio==3.7.1',
    'click==8.1.6',
    'fastapi==0.101.0',
    'h11==0.14.0',
    'httptools==0.6.0',
    'idna==3.4',
    'pydantic==2.1.1',
    'pydantic_core==2.4.0',
    'python-dotenv==1.0.0',
    'PyYAML==6.0.1',
    'sniffio==1.3.0',
    'starlette==0.27.0',
    'typing_extensions==4.7.1',
    'uvicorn==0.23.2',
    'uvloop==0.17.0',
    'watchfiles==0.19.0',
    'websockets==11.0.3',
    'SQLAlchemy==2.0.19',
    'psycopg2-binary==2.9.7',
    'alembic==1.11.2',
    'bcrypt==4.0.1',
    'passlib==1.7.4',
    'python-jose==3.3.0',
    'python-multipart==0.0.6',
    'redis==5.0.0',
    'email-validator==2.0.0.post2',
    'fastapi_storages==0.3.0 '
]

tests_requires = [
    'pytest==7.4.0',
    'pytest-asyncio==0.21.1',
    'httpx==0.24.1'
]

setup(
    name='hack',
    version='0,0',
    description='service application backend',
    classifiers=[
        'Programming Language :: Python',
        'Framework :: FastAPI',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: ASGI :: Application',
    ],
    author='Danila Postnikov',
    author_email='luginich.danil@gmail.com',
    keywords='web fasapi',
    packages=find_packages(),
    include_package_data=True,
    extras_require={
        'testing': tests_requires,
    },
    install_requires=requires,
    entry_points={
        'console_scripts': [
            'check-db-connection = app.console:checkDBConnectionCommand',
            'create-default-admin = app.console:createDefaultAdmin',
        ],
    },
)

