import 'package:urfo/api/base.dart';
import 'package:urfo/index.dart';

class AuthApi extends ApiBase {
  final DioClient client = DioClient();

  Future<Result> login({
    required String username,
    required String password,
  }) async {
    return super.run(
      () async {
        final res = await client.post('/login', data: {
          'username': username,
          'password': password,
        });
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }
}
