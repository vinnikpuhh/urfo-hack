import 'package:urfo/api/base.dart';
import 'package:urfo/index.dart';
import 'package:urfo/models/user/user.dart';

class UserApi extends ApiBase {
  final DioClient client = DioClient();

  Future<Result> createUser({required User user}) async {
    return super.run(
      () async {
        final res = await client.post('/user/create', data: {
          'user': user,
        });
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }

  Future<Result> updateUser({
    required User user,
    required String id,
  }) async {
    return super.run(
      () async {
        final res = await client.put('/user/update/$id', data: {
          'user': user,
        });
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }

  Future<Result> deleteUser({
    required User user,
    required String id,
  }) async {
    return super.run(
      () async {
        final res = await client.delete('/user/delete/$id', data: {
          'user': user,
        });
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }

  Future<Result> getUser({required String id}) async {
    return super.run(
      () async {
        final res = await client.get('/user/$id');
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }

  Future<Result> getListUsers() async {
    return super.run(
      () async {
        final res = await client.get('/user/list');
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }
}
