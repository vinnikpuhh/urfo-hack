import 'package:dio/dio.dart';


abstract class ApiBase {
  Future<Result<T>> run<T>(Future<Result<T>> Function() fn) async {
    try {
      return await fn();
    } on DioException catch (e) {
      if (e.response?.data?['message'] != null) {
        return Result.failure(e.response!.data['message'][0]);
      } else if (e.response?.data?['message'] != null) {
        return Result.failure(e.response!.data['message']);
      } else {
        return Result.failure('Ошибка на беке');
      }
    } catch (e) {
      return Result.failure('Ошибка на беке');
    }
  }
}

class Result<T> {
  final bool success;
  final String? error;
  final T? data;

  Result._({
    required this.success,
    this.error,
    this.data,
  });

  factory Result.success(T data) {
    return Result._(success: true, data: data);
  }

  factory Result.failure(String error) {
    return Result._(success: false, error: error);
  }
}
