import 'package:urfo/api/base.dart';
import 'package:urfo/index.dart';
import 'package:urfo/models/user/user.dart';

class WorkbookPageApi extends ApiBase {
  final DioClient client = DioClient();

  Future<Result> getWorkbookPage({required String id}) async {
    return super.run(
      () async {
        final res = await client.get('/work_book_page/$id');
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }

  Future<Result> getFileWorkbookPage({
    required String id,
  }) async {
    return super.run(
      () async {
        final res = await client.get('/work_book_page/file/$id');
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }

  Future<Result> getWorkbookPages() async {
    return super.run(
      () async {
        final res = await client.get('/work_book_page');
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }

  Future<Result> sendWorkBook({required String page, required String file}) async {
    return super.run(
      () async {
        final res = await client.post('/work_book_page/create', data: {
          'page': page,
          'file': file,
        });
        if (res.statusCode != 200) {
          return Result.failure(res.data['message']);
        }
        return Result.success(res.data);
      },
    );
  }
}
