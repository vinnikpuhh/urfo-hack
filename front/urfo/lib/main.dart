import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:urfo/index.dart';
import 'package:urfo/modules/load_data/cubit/cubit.dart';

void main() async {
  await Storage.init();

  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarIconBrightness: Brightness.dark,
      statusBarBrightness: Brightness.light,
      statusBarColor: Colors.transparent,
      systemNavigationBarColor: Colors.transparent,
    ),
  );
  runApp(
    ModularApp(
      module: AppModule(),
      child: const AppWidget(),
    ),
  );
}

class AppWidget extends StatelessWidget {
  const AppWidget({super.key});

  @override
  Widget build(BuildContext context) {
    Modular.setInitialRoute('/page1');
    return BlocProvider(
      create: (context) => LoadDataCubit(),
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        color: ThemeColor.red,
        title: 'Urfo',
        routerConfig: Modular.routerConfig,
        theme: ThemeData(
          primarySwatch: ThemeColor.mainMaterialColor,
        ),
      ),
    );
  }
}

class AppModule extends Module {
  @override
  void binds(i) {}

  @override
  void routes(r) {
    r.child(
      '/',
      child: (context) => const RootPage(),
      children: [
        ChildRoute(
          '/page1',
          child: (context) => const History(),
        ),
        ChildRoute(
          '/page2',
          child: (context) => const LoadData(),
        ),
        ChildRoute(
          '/page3',
          child: (context) => const Workers(),
        ),
      ],
    );
  }
}

class RootPage extends StatefulWidget {
  const RootPage({super.key});

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeColor.bgGrey,
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.1,
            width: double.infinity,
            color: ThemeColor.white,
            child: Row(
              children: [
                Container(
                  height: double.infinity,
                  width: MediaQuery.of(context).size.height * 0.2,
                  color: ThemeColor.red,
                  padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.02,
                    horizontal: MediaQuery.of(context).size.height * 0.03,
                  ),
                  child: Image.asset(PngIcon.logoRjd),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.015,
                    horizontal: MediaQuery.of(context).size.height * 0.02,
                  ),
                  child: Text(
                    'ТКП',
                    style: StyleText.redText.copyWith(
                        fontSize: MediaQuery.of(context).size.height * 0.035),
                  ),
                ),
                const Spacer(),
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.015,
                    horizontal: MediaQuery.of(context).size.height * 0.02,
                  ),
                  child: Text(
                    'Выйти из профиля',
                    style: StyleText.redText.copyWith(
                        fontSize: MediaQuery.of(context).size.height * 0.02),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(
                MediaQuery.of(context).size.height * 0.01,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    height: MediaQuery.of(context).size.height * 0.8,
                    color: ThemeColor.white,
                    child: SizedBox(
                      height: MediaQuery.of(context).size.width * 0.2,
                      width: double.infinity,
                      child: Column(
                        children: [
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.042,
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.20,
                            width: MediaQuery.of(context).size.height * 0.20,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                MediaQuery.of(context).size.height * 0.2,
                              ),
                              color: ThemeColor.bgGrey,
                            ),
                            child: SvgPicture.asset(SvgIcon.avatar),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01,
                          ),
                          Text(
                            'Дмитрий Охрименко',
                            style: StyleText.blackText.copyWith(
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.025,
                            ),
                          ),
                          Text(
                            '@PointOfNoReturn',
                            style: StyleText.blackText.copyWith(
                              color: ThemeColor.textGrey,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.015,
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.025,
                          ),
                          Container(
                            height: 5.0,
                            width: double.infinity,
                            color: ThemeColor.bgGrey,
                          ),
                          tile(
                            title: 'История',
                            icon: SvgIcon.history,
                            onTap: () {
                              Modular.to.navigate('/page1');
                              setState(() {});
                            },
                            selected: Modular.to.path.contains('/page1'),
                          ),
                          tile(
                            title: 'Загрузка данных',
                            icon: SvgIcon.loadData,
                            onTap: () {
                              Modular.to.navigate('/page2');
                              setState(() {});
                            },
                            selected: Modular.to.path.contains('/page2'),
                          ),
                          tile(
                            title: 'Сотрудники',
                            icon: SvgIcon.workers,
                            onTap: () {
                              Modular.to.navigate('/page3');
                              setState(() {});
                            },
                            selected: Modular.to.path.contains('/page3'),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.height * 0.01,
                  ),
                  SingleChildScrollView(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.78,
                      color: ThemeColor.white,
                      child: const RouterOutlet(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget tile({
    required String title,
    required String icon,
    required void Function() onTap,
    required bool selected,
  }) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.1,
        width: double.infinity,
        padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * 0.01,
        ),
        decoration: BoxDecoration(
            color: selected ? ThemeColor.bgGrey : Colors.transparent),
        child: Row(
          children: [
            Container(
              width: 5.0,
              height: double.infinity,
              color: selected ? ThemeColor.red : Colors.transparent,
            ),
            const SizedBox(width: 5.0),
            Container(
              padding: EdgeInsets.all(
                MediaQuery.of(context).size.height * 0.015,
              ),
              height: MediaQuery.of(context).size.height * 0.07,
              width: MediaQuery.of(context).size.height * 0.07,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  MediaQuery.of(context).size.height * 0.1,
                ),
                color: ThemeColor.bgGrey,
              ),
              child: SvgPicture.asset(
                icon,
                // ignore: deprecated_member_use
                color: selected ? ThemeColor.red : ThemeColor.textGrey,
              ),
            ),
            Container(
              padding: EdgeInsets.all(
                MediaQuery.of(context).size.height * 0.02,
              ),
              width: MediaQuery.of(context).size.height * 0.25,
              child: Text(
                title,
                style: StyleText.blackText.copyWith(
                  fontSize: MediaQuery.of(context).size.height * 0.02,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
