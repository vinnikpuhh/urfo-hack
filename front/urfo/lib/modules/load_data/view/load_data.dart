import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:urfo/index.dart';
import 'package:urfo/modules/load_data/cubit/cubit.dart';
import 'package:urfo/modules/load_data/view/editing_response.dart';

class LoadData extends StatefulWidget {
  const LoadData({super.key});

  @override
  State<LoadData> createState() => _LoadDataState();
}

class _LoadDataState extends State<LoadData> {
  FilePickerResult? result;
  FilePickerResult? result1;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeColor.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () async {
                    result = await FilePicker.platform.pickFiles(
                      type: FileType.custom,
                      allowedExtensions: ['jpg', 'png'],
                    );
                    setState(() {});
                  },
                  child: result?.xFiles == null
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: DottedBorder(
                            child: const SizedBox(
                              height: 300.0,
                              width: double.infinity,
                              child: Center(
                                child: Text(
                                    'Для выбора изображения нажмите в эту область',
                                    style: StyleText.blackText),
                              ),
                            ),
                          ),
                        )
                      : Stack(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child:
                                  Image.file(File(result!.xFiles.first.path)),
                            ),
                            GestureDetector(
                              onTap: () {
                                result = null;
                                setState(() {});
                              },
                              behavior: HitTestBehavior.opaque,
                              child: Container(
                                margin: const EdgeInsets.all(15.0),
                                height: 50.0,
                                width: 50.0,
                                color: ThemeColor.bgGrey,
                                alignment: Alignment.center,
                                child: const Icon(Icons.close),
                              ),
                            ),
                          ],
                        ),
                ),
              ),
            ],
          ),
          GestureDetector(
            onTap: () async {
              result1 = await FilePicker.platform.pickFiles(
                type: FileType.custom,
                allowedExtensions: ['jpg', 'png'],
              );
              setState(() {});
            },
            child: result1?.xFiles == null
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DottedBorder(
                      child: const SizedBox(
                        height: 300.0,
                        width: double.infinity,
                        child: Center(
                          child: Text(
                              'Для выбора изображения нажмите в эту область',
                              style: StyleText.blackText),
                        ),
                      ),
                    ),
                  )
                : Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.file(File(result1!.xFiles.first.path)),
                      ),
                      GestureDetector(
                        onTap: () {
                          result1 = null;
                          setState(() {});
                        },
                        behavior: HitTestBehavior.opaque,
                        child: Container(
                          margin: const EdgeInsets.all(15.0),
                          height: 50.0,
                          width: 50.0,
                          color: ThemeColor.bgGrey,
                          alignment: Alignment.center,
                          child: const Icon(Icons.close),
                        ),
                      ),
                    ],
                  ),
          ),
          const SizedBox(height: 15.0),
          GestureDetector(
            onTap: () {
              BlocProvider.of<LoadDataCubit>(context).openDialog();
              showDialog(
                  context: context,
                  builder: (context) {
                    return EditingResponse();
                  });
            },
            behavior: HitTestBehavior.opaque,
            child: Container(
              height: 80.0,
              margin: EdgeInsets.all(8.0),
              width: double.infinity,
              color: Colors.red,
              alignment: Alignment.center,
              child: Text(
                'Распознать',
                style: StyleText.redText.copyWith(color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
