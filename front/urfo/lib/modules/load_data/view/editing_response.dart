import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:urfo/index.dart';
import 'package:urfo/modules/load_data/cubit/cubit.dart';
import 'package:urfo/modules/load_data/cubit/state.dart';

class EditingResponse extends StatelessWidget {
  const EditingResponse({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoadDataCubit, LoadDataState>(builder: (context, state) {
      if (state.status.loading || state.status.init) {
        return Container(
          padding: const EdgeInsets.all(10.0),
          margin: const EdgeInsets.all(20.0),
          color: ThemeColor.white,
          child: const Center(
            child: CircularProgressIndicator(
              color: ThemeColor.mainMaterialColor,
            ),
          ),
        );
      }
      return Container(
        padding: const EdgeInsets.all(10.0),
        margin: const EdgeInsets.all(20.0),
        color: ThemeColor.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              'Ответ сервиса',
              style: StyleText.blackText,
            ),
            Material(
              child: TextField(
                autofocus: false,
                maxLines: null,
                controller: TextEditingController(
                  text: response,
                ),
              ),
            ),
            /*   */
            /*  Text(response), */
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                Modular.to.navigate('/page1');
              },
              behavior: HitTestBehavior.opaque,
              child: Container(
                height: 80.0,
                width: 500.0,
                color: ThemeColor.red,
                alignment: Alignment.center,
                child: Text(
                  'Сохранить',
                  style: StyleText.redText.copyWith(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
