import 'dart:ui';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'state.freezed.dart';
enum LoadData {
  init,
  loading,
  success,
  failure,
}

extension LoadDataX on LoadData {
  bool get init => this == LoadData.init;
  bool get loading => this == LoadData.loading;
  bool get success => this == LoadData.success;
  bool get failure => this == LoadData.failure;
}

@Freezed()
class LoadDataState with _$LoadDataState {
  factory LoadDataState({
    required LoadData status,
  }) = _LoadDataState;
}
