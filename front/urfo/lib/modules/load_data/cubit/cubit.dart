import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:urfo/api/workbook.dart';
import 'package:urfo/modules/load_data/cubit/state.dart';

class LoadDataCubit extends Cubit<LoadDataState> {
  WorkbookPageApi api = WorkbookPageApi();

  LoadDataCubit()
      : super(LoadDataState(
          status: LoadData.init,
        ));

  void openDialog() async {
    emit(state.copyWith(status: LoadData.loading));
    Future.delayed(Duration(seconds: 5), () {
      emit(state.copyWith(status: LoadData.success));
    });
  }

  void sendAndSaveWorkbook() async {
    emit(state.copyWith(status: LoadData.loading));

    final result = await api.sendWorkBook(page: page, file: file);

    if (!result.success) {
      emit(state.copyWith(status: LoadData.failure));
    }

    emit(state.copyWith(status: LoadData.success));
  }
}
