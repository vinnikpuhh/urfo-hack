// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$LoadDataState {
  LoadData get status => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LoadDataStateCopyWith<LoadDataState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoadDataStateCopyWith<$Res> {
  factory $LoadDataStateCopyWith(
          LoadDataState value, $Res Function(LoadDataState) then) =
      _$LoadDataStateCopyWithImpl<$Res, LoadDataState>;
  @useResult
  $Res call({LoadData status});
}

/// @nodoc
class _$LoadDataStateCopyWithImpl<$Res, $Val extends LoadDataState>
    implements $LoadDataStateCopyWith<$Res> {
  _$LoadDataStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as LoadData,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$LoadDataStateImplCopyWith<$Res>
    implements $LoadDataStateCopyWith<$Res> {
  factory _$$LoadDataStateImplCopyWith(
          _$LoadDataStateImpl value, $Res Function(_$LoadDataStateImpl) then) =
      __$$LoadDataStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({LoadData status});
}

/// @nodoc
class __$$LoadDataStateImplCopyWithImpl<$Res>
    extends _$LoadDataStateCopyWithImpl<$Res, _$LoadDataStateImpl>
    implements _$$LoadDataStateImplCopyWith<$Res> {
  __$$LoadDataStateImplCopyWithImpl(
      _$LoadDataStateImpl _value, $Res Function(_$LoadDataStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
  }) {
    return _then(_$LoadDataStateImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as LoadData,
    ));
  }
}

/// @nodoc

class _$LoadDataStateImpl implements _LoadDataState {
  _$LoadDataStateImpl({required this.status});

  @override
  final LoadData status;

  @override
  String toString() {
    return 'LoadDataState(status: $status)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadDataStateImpl &&
            (identical(other.status, status) || other.status == status));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoadDataStateImplCopyWith<_$LoadDataStateImpl> get copyWith =>
      __$$LoadDataStateImplCopyWithImpl<_$LoadDataStateImpl>(this, _$identity);
}

abstract class _LoadDataState implements LoadDataState {
  factory _LoadDataState({required final LoadData status}) =
      _$LoadDataStateImpl;

  @override
  LoadData get status;
  @override
  @JsonKey(ignore: true)
  _$$LoadDataStateImplCopyWith<_$LoadDataStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

String firstImage = 'assets/fonts/photo_2024-05-18_17-32-10.jpg';
String secondImage = 'assets/fonts/photo_2024-05-18_17-32-11.jpg';
String response = '''
Трудовая книжка - №0572564
Фамилия - Охрименко
Имя - Дмитрий
Отчество - Аркадьевич
Дата рождения - 24.08.1998
Дата заполнения - 15.10.2018

Сведения о работе

Наименование организации - министерство образования и науки челябинской области государственное бюджетное учреждение дополнительного образования "Дом юношеского технического творчества Челябинской области"

1)15.10.2018 - Принят в учебно-методический отдел на должность педагога дополнительного образования - Приказ от 15.10 2018 № 101.2к

2)23.04.2019 - Трудовой договор расторгнут по инициативе работника, пункт З части первой статьи 77 Прудового кодекса Российской Федерации Начальник отдела кадров Васенкина Н.А. - Приказ от 23.04.2019 № 48к 
''';
String firstData = '''
Трудовая книжка - №0572564
Фамилия - Охрименко
Имя - Дмитрий
Отчество - Аркадьевич
Дата рождения - 24.08.1998
Дата заполнения - 15.10.2018''';
String secondData = '''
Сведения о работе

Наименование организации 
- министерство образования 
и науки челябинской области 
государственное бюджетное 
учреждение дополнительного 
образования "Дом юношеского 
технического творчества 
Челябинской области"

1)15.10.2018 - Принят в 
учебно-методический отдел 
на должность педагога 
дополнительного образования 
- Приказ от 15.10 2018 № 101.2к

2)23.04.2019 - Трудовой 
договор расторгнут по 
инициативе работника, 
пункт З части первой 
статьи 77 Прудового 
кодекса Российской 
Федерации Начальник 
отдела кадров Васенкина 
Н.А. - Приказ от 23.04.2019 
№ 48к 
''';

String page = 'first';
String file = 'file';
