import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:urfo/index.dart';
import 'package:urfo/modules/load_data/cubit/cubit.dart';

class History extends StatefulWidget {
  const History({super.key});

  @override
  State<History> createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeColor.white,
      child: Column(
        children: [
          tile(
            date: '19.05.2024',
            name: 'Охрименко Дмитрий Аркадьевич',
            role: 'Инженер-программист',
          ),
        ],
      ),
    );
  }

  Widget tile({
    required String date,
    required String name,
    required String role,
  }) {
    return GestureDetector(
      onTap: () {
        BlocProvider.of<LoadDataCubit>(context).openDialog();
        showAdaptiveDialog(
            context: context,
            builder: (context) {
              return const Detail();
            });
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 5.0),
        height: MediaQuery.of(context).size.height * 0.12,
        width: double.infinity,
        color: ThemeColor.lightGrey,
        padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * 0.015,
          horizontal: MediaQuery.of(context).size.height * 0.04,
        ),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  name,
                  style: StyleText.blackText.copyWith(
                      fontSize: MediaQuery.of(context).size.height * 0.033),
                ),
                Text(
                  role,
                  style: StyleText.blackText.copyWith(
                      fontSize: MediaQuery.of(context).size.height * 0.023),
                ),
              ],
            ),
            const Spacer(),
            Text(
              date,
              style: StyleText.blackText.copyWith(
                  fontSize: MediaQuery.of(context).size.height * 0.023),
            ),
          ],
        ),
      ),
    );
  }
}
