import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:urfo/index.dart';
import 'package:urfo/modules/load_data/cubit/cubit.dart';
import 'package:urfo/modules/load_data/cubit/state.dart';

class Detail extends StatelessWidget {
  const Detail({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoadDataCubit, LoadDataState>(
      builder: (context, state) {
        if (state.status.loading || state.status.init) {
          return Container(
            padding: const EdgeInsets.all(10.0),
            margin: const EdgeInsets.all(20.0),
            color: ThemeColor.white,
            child: const Center(
              child: CircularProgressIndicator(
                color: ThemeColor.mainMaterialColor,
              ),
            ),
          );
        }
        return Container(
          padding: const EdgeInsets.all(10.0),
          margin: const EdgeInsets.all(10.0),
          color: ThemeColor.white,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                      onTap: () => Navigator.pop(context),
                      behavior: HitTestBehavior.opaque,
                      child: Container(
                        height: 50.0,
                        width: 50.0,
                        color: ThemeColor.bgGrey,
                        alignment: Alignment.center,
                        child: Icon(Icons.close),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Image.asset(
                      firstImage,
                      width: MediaQuery.of(context).size.width * .5,
                    ),
                    const SizedBox(width: 20.0),
                    Column(
                      children: [
                        Text(
                          firstData,
                          style: StyleText.blackText,
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 5.0),
                Row(
                  children: [
                    Image.asset(
                      secondImage,
                      width: MediaQuery.of(context).size.width * .5,
                    ),
                    const SizedBox(width: 20.0),
                    Column(
                      children: [
                        Text(
                          softWrap: true,
                          secondData,
                          style: StyleText.blackText,
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
