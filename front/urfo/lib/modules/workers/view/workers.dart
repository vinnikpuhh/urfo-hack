import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:urfo/index.dart';

class Workers extends StatefulWidget {
  const Workers({super.key});

  @override
  State<Workers> createState() => _WorkersState();
}

class _WorkersState extends State<Workers> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeColor.white,
      child: Column(
        children: [
          tile(
            date: '01.08.22',
            name: 'Надина Георгиевна Замещук',
            role: 'Гл.администратор',
          ),
          tile(
            date: '08.03.23',
            name: 'Виктор Олегович Григорян',
            role: 'Администратор',
          ),
          tile(
            date: '26.04.23',
            name: 'Фёдор Сергеевич Мурашкин',
            role: 'Начальник отдела',
          ),
          tile(
            date: '31.06.24',
            name: 'Мария Петровна Олещук',
            role: 'Сотрудник',
          ),
        ],
      ),
    );
  }

  Widget tile({
    required String date,
    required String name,
    required String role,
  }) {
    return Container(
      margin: const EdgeInsets.only(bottom: 5.0),
      height: MediaQuery.of(context).size.height * 0.12,
      width: double.infinity,
      color: ThemeColor.lightGrey,
      padding: EdgeInsets.symmetric(
        vertical: MediaQuery.of(context).size.height * 0.015,
        horizontal: MediaQuery.of(context).size.height * 0.04,
      ),
      child: Row(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.09,
            width: MediaQuery.of(context).size.height * 0.09,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: ThemeColor.bgGrey,
              borderRadius: BorderRadius.circular(
                  MediaQuery.of(context).size.height * 0.1),
            ),
            padding: EdgeInsets.all(
              MediaQuery.of(context).size.height * 0.02,
            ),
            child: SvgPicture.asset(
              SvgIcon.avatar,
            ),
          ),
          SizedBox(width: MediaQuery.of(context).size.height * 0.032),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                date,
                style: StyleText.blackText.copyWith(
                    fontSize: MediaQuery.of(context).size.height * 0.023),
              ),
              Text(
                name,
                style: StyleText.blackText.copyWith(
                    fontSize: MediaQuery.of(context).size.height * 0.033),
              ),
            ],
          ),
          const Spacer(),
          Text(
            role,
            style: StyleText.blackText
                .copyWith(fontSize: MediaQuery.of(context).size.height * 0.023),
          ),
        ],
      ),
    );
  }
}
