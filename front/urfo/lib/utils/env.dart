enum EnvironmentType { production, development, custom }

class Environment {
  static const EnvironmentType env = EnvironmentType.production;

  static String getApi() => env == EnvironmentType.production
      ? "http://31.128.39.68:8000/"
      : "http://31.128.39.68:8000/";
}
