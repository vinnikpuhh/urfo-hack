import 'package:flutter/material.dart';

class ThemeColor {
  static const lightGrey = Color(0xffA3A3A3);
  static const red = Color(0xffEF3122);
  static const bgGrey = Color(0xffD9DBDF);
  static const textGrey = Color(0xff767D89);
  static const textBlack = Colors.black;
  static const white = Colors.white;

  static const int mainColor = 0xffEF3122;
  static const MaterialColor mainMaterialColor = MaterialColor(
    mainColor,
    <int, Color>{
      50: Color.fromRGBO(249, 246, 240, .1),
      100: Color.fromRGBO(249, 246, 240, .2),
      200: Color.fromRGBO(249, 246, 240, .3),
      300: Color.fromRGBO(249, 246, 240, .4),
      400: Color.fromRGBO(249, 246, 240, .5),
      500: Color.fromRGBO(249, 246, 240, .6),
      600: Color.fromRGBO(249, 246, 240, .7),
      700: Color.fromRGBO(249, 246, 240, .8),
      800: Color.fromRGBO(249, 246, 240, .9),
      900: Color.fromRGBO(249, 246, 240, 1),
    },
  );
}

class StyleText {
  static const TextStyle redText = TextStyle(
    color: ThemeColor.red,
    fontWeight: FontWeight.w500,
    fontSize: 40.0,
    fontFamily: "Verdana",
  );
  static const TextStyle blackText = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontSize: 25.0,
    fontFamily: "Verdana",
  );
}

class SvgIcon {
  static String history = 'assets/svg/history.svg';
  static String loadData = 'assets/svg/load_data.svg';
  static String workers = 'assets/svg/workers.svg';
  static String avatar = 'assets/svg/avatar.svg';
}

class PngIcon {
  static String logoRjd = 'assets/png/logo_rjd.png';
}
